import React, {useContext} from "react";
import context from "../../../context/context";
import CartItem from '../CartItem'
import './Cart.css'
const Cart = () =>{
    const {contextState:{cart}, delFromCart, decreaseFromCart, increaseFromCart} = useContext(context)
    const cartElements = cart.map((item, index) => <CartItem
        decreaseItem={()=>decreaseFromCart(index)}
        increaseItem={()=>increaseFromCart(index)}
        deleteItem={()=>delFromCart(index)}
        {...item} />)
    let totalPrice = 0;
    cart.forEach(({count, price})=>totalPrice+=count*price)
    return(
        <>
            <table className="show-cart table">
                {cartElements}
            </table>
            <div>Total price: ${totalPrice}</div>
         </>
    )
}
export default Cart
