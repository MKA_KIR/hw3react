import React, {useContext} from "react";
import Button from "../../../../Shared/Components/Button";
import './ProductCard.css'
import context from "../../../../context/context";
const ProductCard = (product) =>{
    const {src, title, price} = product;
    const {addToCart} = useContext(context)
    return(
        <div className="col">
            <div className="card product-card">
                <img className="card-img-top" src={src} alt="Card image cap"/>
                    <div className="card-block">
                        <h4 className="card-title">{title}</h4>
                        <p className="card-text">Price: ${price}</p>
                        <Button type='primary' text='Add To Cart' handleClick={()=>addToCart(product)}/>
                    </div>
            </div>
        </div>
    )
}
export default ProductCard