import React,{useContext}from "react";
import ProductCard from "../../../Products/components/ProductCard";
import context from "../../../../context/context";


const ProductsListPage = () => {
    const {contextState:{productList}} = useContext(context)
    const productsElements = productList.map(item => <ProductCard {...item} /> );
    return (
        <div className='container'>
            <div className='row'>
                {productsElements}
        </div>
        </div>
    )
}

export default ProductsListPage;