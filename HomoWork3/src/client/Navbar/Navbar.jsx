import React, {useContext} from "react";
import './Navbar.css';
import Button from "../../Shared/Components/Button";
import {componentsProps} from "./componentsProps";
import context from "../../context/context";
const Navbar = () =>{
    const {openCartModal, clearCart, contextState:{cart}} = useContext(context)
    const {openCartButton, clearCartButton} = componentsProps;
    let totalCartProducts = 0;
    cart.forEach(({count})=>totalCartProducts+=count);
    return(
        <div className="navbar">
            <div className="container">
                <div className="navbar-row">
                    <Button {...openCartButton}  text={`Cart (${totalCartProducts})`} handleClick={openCartModal} />
                    <Button {...clearCartButton} handleClick={clearCart}/>
                </div>
            </div>
        </div>
    )
}
export default Navbar