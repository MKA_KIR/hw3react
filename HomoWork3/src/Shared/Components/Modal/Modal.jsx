import React, {useContext} from "react";
import Button from "../Button";
import context from "../../../context/context";
import "./Modal.css"
const Modal = ({title, children}) =>{
    const {contextState:{modalCartOpen}, closeCartModal} = useContext(context)
    const display = modalCartOpen ? 'show' : ''

    return(
            <div className={`modal-dialog modal-lg ${display}`} role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">{title}</h5>
                        <Button type="button" text='&times;' handleClick={closeCartModal}/>
                    </div>
                    <div className="modal-body">
                        {children}
                    </div>
                    <div className="modal-footer">
                        <Button type="button" text='Close' handleClick={closeCartModal}/>
                    </div>
                </div>
            </div>
    )
}
export default Modal