import React, {useReducer} from "react";
import context from "./context";
import {initialState} from "./initialState";
import {contextReducer} from './contextReducer'
import {OPEN_CART_MODAL,CLOSE_CART_MODAL,CLEAR_CART, ADD_TO_CART, DEL_FROM_CART, INCREASE_FROM_CART, DECREASE_FROM_CART} from './contextConstants'

const ProviderContext = ({children}) =>{
    const [contextState, dispatch] = useReducer(contextReducer, initialState);
console.log(contextState)
    const contextValue = {
        contextState,
        closeCartModal: () => dispatch ({type:CLOSE_CART_MODAL}),
        openCartModal: () => dispatch ({type:OPEN_CART_MODAL}),
        clearCart: () => dispatch ({type:CLEAR_CART}),
        addToCart: (product) => dispatch({type:ADD_TO_CART, payload:product}) ,
        delFromCart: (idx) => dispatch({type:DEL_FROM_CART, payload:idx}),
        increaseFromCart: (idx) => dispatch({type: INCREASE_FROM_CART, payload: idx}),
        decreaseFromCart: (idx) => dispatch({type: DECREASE_FROM_CART, payload: idx}),
    }
    return(
        <context.Provider value={contextValue}>{children}</context.Provider>
    )
}
export default ProviderContext