import {productList} from "./productList";
const cartString = localStorage.getItem('cart') || "[]";
export const initialState = {
    cart:JSON.parse(cartString),
    productList,
    modalCartOpen: false,
}


