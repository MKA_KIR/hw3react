import React from 'react';
import Navbar from '../src/client/Navbar'
import ProductsListPage from '../src/client/Products/pages/ProductsListPage'
import ProviderContext from "./context/Provider.Context";
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Cart from "./client/components/Cart";
import Modal from "./Shared/Components/Modal";


function App() {
  return (
      <ProviderContext>
    <div className="App">
        <Navbar />
      <ProductsListPage />
        <Modal title='Cart'>
            <Cart/>
        </Modal>
    </div>
          </ProviderContext>
  );
}

export default App;
